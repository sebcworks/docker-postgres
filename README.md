PostgreSQL 9.4/9.5 Container
============================

Description
-----------

Yet another PostgreSQL container.

Tag "latest" equals to tag pg94 (for postgresql 9.4). To use postgresql 9.5, use tag pg95

It is based on the [official PostgreSQL container](https://registry.hub.docker.com/_/postgres/).

This version use the volumes created by the data container sebcworks/postgres-data (https://registry.hub.docker.com/u/sebcworks/postgres-data/) :

* /data/postgresql
* /var/log

I also do not expose the port 5432 as I intend to use the container by linking (the option `--link`)

Usage
-----

*If used with my "postgres-data" container*

    docker run --name pgdata sebcworks/postgres-data
    
    docker run --name pgdb -e POSTGRES_PASSWORD=tobechanged --volumes-from pgdata -d sebcworks/postgres:pg95
    
    docker exec -it pgdb bash
    su postgres
    psql
    ALTER USER postgres WITH ENCRYPTED PASSWORD 'toto';

Then it can be linked in an app who needs postgresql:
`docker run --name myapp --link pgdb:postgres -d username/app`
